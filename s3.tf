resource "aws_s3_bucket" "mynav-terraform-s3-bucket" {
  bucket = "my-tf-poc-bucket"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}
resource "aws_s3_bucket_acl" "mynav-terraform-s3-bucket" {
  bucket = aws_s3_bucket.mynav-terraform-s3-bucket.id
  acl    = "private"
}