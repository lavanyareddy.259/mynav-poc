provider "aws" {
   region     = "us-east-1"
   access_key = var.access_key
   secret_key = var.secret_key
}

module "webserver-1" {
  source = "./terraform/project-01"
}



